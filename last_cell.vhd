library ieee;
use ieee.std_logic_1164.all;

entity last_cell is
    port(y: in std_logic_vector(3 downto 0);
         z: out std_logic);
end last_cell;

architecture last of last_cell is
begin
    z <= (not y(2) and y(1) and not y(0)) or (y(3) and y(0));
end last;
