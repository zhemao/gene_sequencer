library ieee;
use ieee.std_logic_1164.all;

entity gene_sequencer is
    port(x1: in std_logic_vector(0 to 19);
         x0: in std_logic_vector(0 to 19);
         z: out std_logic_vector(0 to 19));
end gene_sequencer;

architecture sequencer of gene_sequencer is
    signal y3: std_logic_vector(0 to 19);
    signal y2: std_logic_vector(0 to 19);
    signal y1: std_logic_vector(0 to 19);
    signal y0: std_logic_vector(0 to 19);

    component first_cell
        port(x: in std_logic_vector(1 downto 0);
             ynext: out std_logic_vector(2 downto 0));
    end component;

    component second_cell
        port(y: in std_logic_vector(2 downto 0);
             x: in std_logic_vector(1 downto 0);
             ynext: out std_logic_vector(3 downto 0);
             z: out std_logic);
    end component;

    component normal_cell
        port(y: in std_logic_vector(3 downto 0);
             x: in std_logic_vector(1 downto 0);
             ynext: out std_logic_vector(3 downto 0);
             z: out std_logic);
    end component;

    component last_cell
        port(y: in std_logic_vector(3 downto 0);
             z: out std_logic);
    end component;
begin
    stage0: first_cell port map(x(1) => x1(0),
                                x(0) => x0(0),
                                ynext(2) => y2(0),
                                ynext(1) => y1(0),
                                ynext(0) => y0(0));
    stage1: second_cell port map(y(2) => y2(0),
                                 y(1) => y1(0),
                                 y(0) => y0(0),
                                 x(1) => x1(1),
                                 x(0) => x0(1),
                                 ynext(3) => y3(1),
                                 ynext(2) => y2(1),
                                 ynext(1) => y1(1),
                                 ynext(0) => y0(1),
                                 z => z(0));

    normal_gen:
    for i in 2 to 19 generate
        stagex: normal_cell port map(y(3) => y3(i-1),
                                     y(2) => y2(i-1),
                                     y(1) => y1(i-1),
                                     y(0) => y0(i-1),
                                     x(1) => x1(i),
                                     x(0) => x0(i),
                                     ynext(3) => y3(i),
                                     ynext(2) => y2(i),
                                     ynext(1) => y1(i),
                                     ynext(0) => y0(i),
                                     z => z(i-1));
    end generate;

    stage20: last_cell port map(y(3) => y3(19),
                                y(2) => y2(19),
                                y(1) => y1(19),
                                y(0) => y0(19),
                                z => z(19));

end sequencer;
