library ieee;
use ieee.std_logic_1164.all;

entity tutorial is 
	port( x1, x2, x3 : in std_logic; 
	      f : out std_logic);
end tutorial;

architecture mux of tutorial is
begin
	f <= (x1 and x2) or (x3 and not x2);
end mux;