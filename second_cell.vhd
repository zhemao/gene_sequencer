library ieee;
use ieee.std_logic_1164.all;

entity second_cell is
    port(y: in std_logic_vector(2 downto 0);
         x: in std_logic_vector(1 downto 0);
         ynext: out std_logic_vector(3 downto 0);
         z: out std_logic);
end second_cell;

architecture second of second_cell is
    signal a, t: std_logic;
begin
    a <= not x(1) and not x(0);
    t <= x(1) and x(0);

    z <= '0';

    ynext(3) <= y(2) and not y(1) and not y(0) and a;
    ynext(2) <= (y(2) and not y(0) and a) or (not y(0) and t) or
                (not y(2) and y(1) and y(0) and a) or 
                (y(2) and not y(1) and y(0) and a) or
                (y(1) and y(0) and t) or (y(2) and y(0) and t);
    ynext(1) <= (not y(2) and t) or (y(1) and not y(0) and t) or
                (y(2) and y(1) and not y(0) and a) or 
                (not y(2) and y(1) and y(0) and not x(0)) or
                (y(2) and not y(1) and y(0) and x(1));
    ynext(0) <= (not y(2) and a) or (y(1) and a) or 
                (y(2) and not y(1) and not y(0) and t) or
                (y(2) and y(1) and y(0) and t) or 
                (not y(2) and not y(1) and y(0) and t);
end second;
