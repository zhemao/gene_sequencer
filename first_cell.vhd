library ieee;
use ieee.std_logic_1164.all;

entity first_cell is
    port(x: in std_logic_vector(1 downto 0);
         ynext: out std_logic_vector(2 downto 0));
end first_cell;

architecture first of first_cell is
begin
    ynext(2) <= x(1) and x(0);
    ynext(1) <= x(1) and x(0);
    ynext(0) <= not x(1) and not x(0);
end first;
